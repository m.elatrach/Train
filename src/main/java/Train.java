
import java.util.HashMap;

public class Train {

    public static final String HEAD_TEXT = "<HHHH:";
    public static final String PASSENGER_TEXT = ":|OOOO|:";
    public static final String EMPTY_CONTAINER_TEXT = ":|____|:";
    public static final String FULL_CONTAINER_TEXT = ":|^^^^|:";
    public static final String RESTAURANT_TEXT = ":|hThT|:";

    private String trainStructure;
    private String trainText = "";

    private static HashMap<Character,String> trainCarsTypes = new HashMap<Character, String>();

    static {
        initTrainCarsTypes();
    }
    static void initTrainCarsTypes() {
        trainCarsTypes.put('H', HEAD_TEXT);
        trainCarsTypes.put('P', PASSENGER_TEXT);
        trainCarsTypes.put('R', RESTAURANT_TEXT);
        trainCarsTypes.put('C', EMPTY_CONTAINER_TEXT);
        trainCarsTypes.put('F', FULL_CONTAINER_TEXT);
    }

    public Train(String trainStructure) {

        this.trainStructure=trainStructure.toUpperCase();

    }

    public StringBuilder buildTrain() {

        StringBuilder trainText = new StringBuilder();
        char[] trainCars = trainStructure.toCharArray();

        for (char trainCar:trainCars ) {
            trainText.append(trainCarsTypes.get(trainCar));
        }
        removeTailOfTheTrain(trainText);
       return trainText;

    }
    public StringBuilder removeTailOfTheTrain( StringBuilder trainText){

         trainText.setLength(trainText.length()-1);
         return trainText;
    }



    public String print() {
        trainText = buildTrain().toString();
        return trainText ;
    }

    public boolean fill() {
        if(isAnyContainerEmpty()){
            trainStructure = trainStructure.replaceFirst("C","F");
            print();
            return true;
        }

        return false;

    }

    private boolean isAnyContainerEmpty(){
        return this.trainStructure.contains("C");
    }
}
